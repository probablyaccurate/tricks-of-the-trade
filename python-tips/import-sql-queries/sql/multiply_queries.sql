-- get all ID's from the super_store data set
select id from super_store;

-- get the top 10 names from the super_store data set
select name from super_store limit 10;
