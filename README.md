# Tricks of the Trade

This repo is meant to be a helpful resource to share workflow tips, useful code snippets, and any other resources that can help you when working with your projects.
