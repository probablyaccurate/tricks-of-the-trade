# TODO

- [x] Set up Slack for _Probably Accurate_
- [x] Set up BitBucket repo for _Probably Accurate_
- [ ] Create the [tricks-of-the-trade](https://bitbucket.org/probablyaccurate/tricks-of-the-trade) repo
- [ ] Create a easy to use set up workflow
  - [ ] PostgreSQL App
  - [ ] Provide scripts to build the data bases
  - [ ] Write instructions
